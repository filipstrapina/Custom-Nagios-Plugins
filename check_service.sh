#!/bin/bash

service $1 status > /dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "OK - $1 is running"
    exit 0
else
    echo "CRITICAL - $1 is not running"
    exit 2
fi
